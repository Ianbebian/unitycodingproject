﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManagerScript : MonoBehaviour {

    private const float TILE_SIZE = 1.0f;
    private const float TILE_OFFSET = 0.5f;

    public List<GameObject> playerPrefabs;
    private GameObject activePlayer;

    public static int selectionX = -1;
    public static int selectionY = -1;

    // Update is called once per frame
    private void Update () {
        UpdateSelection();
        DrawChessboard();

        /**
        if (Input.GetKey("up"))
        {
            selectionY++;
        }
        else if (Input.GetKey("down"))
        {
            selectionY--;
        }
        else if (Input.GetKey("left"))
        {
            selectionX--;
        }
        else if (Input.GetKey("right"))
        {
            selectionX++;
        }
        **/
    }

    private void UpdateSelection ()
    {
        if (!Camera.main)
            return;

        // Camera positioning
        /* //TODO */
        /* Create make the camera centre on the cursor of the board */
        RaycastHit hit;
        if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("GridPlane")))
        {
            selectionX = (int) hit.point.x;
            selectionY = (int) hit.point.z;
            
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        }
    }




    private void DrawChessboard()
    {
        Vector3 widthLine = Vector3.right * 8;
        Vector3 heightLine = Vector3.forward * 8;

        for (int i = 0; i < 9; i++)
        {
            Vector3 start = Vector3.forward * i;
            Debug.DrawLine(start, start + widthLine);
            for (int j = 0; j < 9; j++)
            {
                start = Vector3.right * j;
                Debug.DrawLine(start, start + heightLine);
            }
        }
        // Draw the selection
        /**
        if (selectionX >= 0 && selectionY >=0)
        {
            Debug.DrawLine(
                Vector3.forward * selectionY + Vector3.right * selectionX,
                Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));

            Debug.DrawLine(
               Vector3.forward * (selectionY + 1) + Vector3.right * selectionX ,
               Vector3.forward * selectionY + Vector3.right * + (selectionX + 1));
        }
        **/
    }

}
