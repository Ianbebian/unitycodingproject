﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy_script : MonoBehaviour {

    public AudioClip hitSound;

    private AudioSource source;

    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            source.PlayOneShot(hitSound, 0.5F);
            Destroy(this.gameObject, 2f);
        }
        
    }
}
