﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorScript : MonoBehaviour {


    private RaycastHit hit;

    
    void Update () {
        
        // Move the cursor over the square
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), 
            out hit, 25.0f, LayerMask.GetMask("GridPlane")))
        {          
            transform.position = new Vector3((int)hit.point.x + 0.5f, 0.041f , (int)hit.point.z + 0.5f);    
        }
        

    }
}
