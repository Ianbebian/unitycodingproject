﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_script : MonoBehaviour
{
    public Transform target;
    public float speed;


    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;

    public float shotSpeed;

    public AudioClip shootSound;
    public AudioClip teleportSound;

    private Vector3 newDir;
    private float nextFire;
    private AudioSource source;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    /**
     * //  TODO //
     * If the player presses “Q”.
	   Re-center the cursor & the camera to the player.
        (@Krister, having trouble implementing)
     */
    private void Update()
    {
        // Look at the target and draw an line towards the 
            Vector3 targetDir = target.position - transform.position;
            float step = speed * Time.deltaTime;
            newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
            Debug.DrawRay(transform.position, newDir, Color.magenta);
            newDir.y = 0.0f;
            transform.rotation = Quaternion.LookRotation(newDir);


        // Shoot magic ball, Enemies hit by a sorcerer's bolt will die and disappear.
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            float vol = Random.Range(0.3F, 0.5F);
            source.PlayOneShot(shootSound, vol);
            nextFire = Time.time + fireRate;
            GameObject bullet = Instantiate(shot, shotSpawn.position, shotSpawn.rotation) as GameObject;
            // GameObject bullet = Instantiate(shot, shotSpawn.position, Vector3(45,45,45)) as GameObject;
            bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * shotSpeed;
            Destroy(bullet, 2.0f);
        }
        /*  If the player presses “E” or Right click.
            The player moves to the cursor location
            
            (@Krister, due to testing feedback. 
                players felt more comfortable by using a mouse, rather
                than compact keyboard controls)
        */
        else if (Input.GetButton("Fire2") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            source.PlayOneShot(teleportSound, 0.5f);
            transform.position = target.position;
        }

    }
}